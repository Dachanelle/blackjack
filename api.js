const { Router } = require("express");
const apiRouter = Router();

const Gambler = require("./models/Gambler");

apiRouter.get("/user/:id", async (req, res) => {
  try {
    const searchedGambler = await Gambler.findById(req.params.id);
    const pojoGambler = searchedGambler.toObject();
    delete pojoGambler.password;
    console.log("2", pojoGambler);
    res.json(pojoGambler);
  } catch (err) {
    console.error(err);
    res.sendStatus(400);
  }
});

apiRouter.post("/user/", async (req, res) => {
  try {
    const newGambler = await Gambler({ ...req.body });
    await newGambler.save();
    res.json({ id: newGambler._id });
  } catch (err) {
    console.error(err);
    res.sendStatus(400);
  }
});

module.exports = apiRouter;
