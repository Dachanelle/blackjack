require("dotenv").config();
const express = require("express");
const path = require("path");
const app = express();
const morgan = require("morgan");
const api = require("./api");
const port = 3023;
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

app.use(
  morgan("dev"),
  bodyParser.json({ extended: false }),
  bodyParser.urlencoded({ extended: false })
);

app.use(express.static("public"), express.static("dist"));
app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/index.html"));
});
app.get("/table", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/table.html"));
});
app.use("/api", api);
async function main() {
  if (process.env.MODE == "production") {
    await mongoose.connect(`mongodb://192.168.171.67:27017/nic`, {
      useNewUrlParser: true,
      authSource: "admin",
      user: "nic",
      pass: process.env.MONGO_PASSWORD,
    });
  } else {
    await mongoose.connect("mongodb://localhost:27017/goose");
  }

  app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
  });
}
main().catch((err) => console.error(err));
